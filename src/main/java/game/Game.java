package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        // Need to be implemented
        if (this.guessResult.containsValue("4A0B") || this.guessResult.size() >= 6) {
            return true;
        }
        return false;
    }
    
    public String getResult() {
        // Need to be implemented
        Set<String> keySet = this.guessResult.keySet();
        StringBuilder result = new StringBuilder();

        for (Iterator<String> it = keySet.iterator(); it.hasNext(); ) {
            String key = (String) it.next();
            String value = this.guessResult.get(key);
            result.append(key + " " + value);
            if (it.hasNext()) {
                result.append("\n");
            }
        }

        if (this.guessResult.containsValue("4A0B")) {
            result.append("\n" + "Congratulations, you win!");
        } else if (this.guessResult.size() == 6){
            result.append("\n" + "Unfortunately, you have no chance, the answer is " + this.answer + "!");
        }
        return result.toString();
    }
    
    private int getBSize(List<Integer> numbers) {
        // Need to be implemented
        int allNumbers = numbers.stream().filter(element -> answer.getAnswer().contains(element))
                .collect(Collectors.toList()).size();
        int sizeForB = allNumbers - this.getASize(numbers);
        return sizeForB;
    }
    
    private int getASize(List<Integer> numbers) {
        // Need to be implemented
        int count = 0;
        for (int i = 0; i < numbers.size(); i++) {
            if (numbers.get(i) == answer.getAnswer().get(i)) {
                count++;
            }
        }
        return count;
    }
}
