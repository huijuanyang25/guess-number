package game;

import java.util.*;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;
    
    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }
    
    
    public String getResult() {
        // Need to be implemented
        Set<String> keySet = this.guessRecord.keySet();
        StringBuilder result = new StringBuilder();
        for (Iterator<String> it = keySet.iterator(); it.hasNext(); ) {
            String key = (String) it.next();
            String value = this.guessRecord.get(key);
            result.append(key + " " + value);
            if (it.hasNext()) {
                result.append("\n");
            }
        }
        return result.toString();
    }
    
    public GameResult getGameResult() {
        // Need to be implemented
        if (this.guessRecord.containsValue("4A0B")) {
            return GameResult.WIN;
        } else if (!this.guessRecord.containsValue("4A0B") && this.guessRecord.size() == 6) {
            return GameResult.LOST;
        } else {
            return GameResult.NORMAL;
        }
    }
}
