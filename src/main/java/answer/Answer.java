package answer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    
    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        // Need to be implemented
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
                ClassLoader.getSystemClassLoader().getResourceAsStream(String.valueOf(filePath))));
        String allLines = bufferedReader.readLine();
        return Arrays.stream(allLines.split(""))
                .map(element -> Integer.parseInt(element))
                .collect(Collectors.toList());
    }

    private List<Integer> generateRandomAnswer() {
        // Need to be implemented
        Random randomNumber = new Random();
        Set<Integer> randomNumberSet = new HashSet<>();
        while (randomNumberSet.size() < 4) {
            randomNumberSet.add(randomNumber.nextInt(10));
        }
        List<Integer> randomAnswer = new ArrayList<>(randomNumberSet);
        return randomAnswer;
    }
    
    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        // Need to be implemented
        if (answer.stream().filter(element -> element > 9).collect(Collectors.toList()).size() != 0
                || answer.size() != 4
                || answer.stream().distinct().collect(Collectors.toList()).size() < 4) {
            throw new InvalidAnswerException("Wrong input");
        }
    }
}
